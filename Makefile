ORCH = orch
NODE = node
CC = gcc
FLAGS = -g -Wall -Wextra -Werror
SRC_REP = src
INCLUDE_REP = include
O_REP = obj
BIN_REP = bin
DOC_REP = doc

S_ORCH  := $(SRC_REP)/worker.c $(SRC_REP)/orch.c
S_NODE  := $(SRC_REP)/node.c
O_ORCH  := $(S_ORCH:$(SRC_REP)/%.c=$(O_REP)/%.o)
O_NODE  := $(S_NODE:$(SRC_REP)/%.c=$(O_REP)/%.o)

# ORCHESTRATOR
$(BIN_REP)/$(ORCH): $(O_ORCH) bin $(BIN_REP)/$(NODE)
	$(CC) $(FLAGS) -I$(INCLUDE_REP) $(O_ORCH) -o $@

# NODE
$(BIN_REP)/$(NODE): $(O_NODE)
	$(CC) $(FLAGS) -I$(INCLUDE_REP) $(O_NODE) obj/worker.o -o $@

# ORCH .o FILES
$(O_ORCH): $(O_REP)/%.o : $(SRC_REP)/%.c obj $(O_NODE)
	$(CC) $(FLAGS) -I$(INCLUDE_REP) -c $< -o $@

# NODE .o FILES
$(O_NODE): $(O_REP)/%.o : $(SRC_REP)/%.c
	$(CC) $(FLAGS) -I$(INCLUDE_REP) -c $< -o $@

bin:
	mkdir -p $(BIN_REP)

obj:
	mkdir -p $(O_REP)

docs:
	doxygen Doxyfile

clean-docs:
	rm -rf $(DOC_REP)/html

clean:
	rm -rf $(O_REP)
	rm -rf $(BIN_REP)
	rm -rf $(DOC_REP)

valgrind:
	make && valgrind $(BIN_REP)/$(ORCH) 2000
