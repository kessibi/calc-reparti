.TH CALC-REPARTI 7
.SH NAME
calculateur reparti \- calculate f(1, 2, .., n) with an orchestrator and workers.
each worker only knows one function f.
.SH SYNOPSIS
  ./bin/orch [\fIPORT\fR]

  ./bin/node [\fIIP\fR] [\fIPORT\fR] [\fIOPERATION\fR]

.SH DESCRIPTION
This program will read calculations from the standard input and give them to a 
worker that can handle them. If no worker can handle a specific calculation,
the orchestrator will simply deny it.
Otherwise the worker will reply after a random amount of time between 0 and 10
seconds.

.SH EXAMPLE USAGE
orchestrator> -(10,2,3) will be interpreted as 10 - 2 - 3
