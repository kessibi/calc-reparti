# calc-reparti / distributed computing

This is a University project that was made in C.

It works with an orcherstrator/nodes scheme.

One orchestrator takes all user inputs and gives them to appropriate nodes.
A node only knows one operation and can only perform one calculation at a time.
(required by subject).

The whole system uses the UDP protocol for sharing information and the orchestrator
can be accessed with the IPv4 and IPv6 protocols.

Run `make` to compile the code and test the whole protocol with `./bin/orch <port>`
and as many instances of `./bin/node <orch ip> <orch port> <operation>`

You can quit the orchestrator at any time by typing `quit` in stdin.
