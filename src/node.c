/**
 * \file worker.c
 * \author Guillaume K. & Chloe R.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <time.h>
#include "../include/worker.h"

/**
 * \def BUFFER_S
 * \brief Size of the buffers used to send/receive informations between sockets
 */
#define BUF_SIZE 4096

/**
 * \fn int* calculate (char* calcul, char operation)
 * \brief Calculates the string depending on the given operation symbol
 * \param calcul Calculation to realise
 * \param operation Specified operation of the calculation
 */
int* calculate (char* calcul, char operation) {
	char calc_op;
	int calc = 1,  s, x;
	int *result = malloc(sizeof(int));
	if(!result) raler("malloc");

	if(sscanf(calcul, "%c(%d,%d%s",
				&calc_op, result, &x, calcul) != 4) {
		fprintf(stderr, "The operation contains non valid numbers.\n");
		return NULL;
	} else if(operation != calc_op){
		fprintf(stderr, "The operation can not be handled by this node.\n");
		return NULL;
	}
	while(calc) {
		switch(operation) {
			case '+' : *result += x;
								 break;
			case '-' : *result -= x;
								 break;
			case '*' : *result *= x;
								 break;
			default : *result /= x;
		}
		if((s = sscanf(calcul, ",%d%s", &x, calcul)) != 2){
			calc = 0;
			if(strcmp(calcul, ")")) {
				fprintf(stderr, "The operation contains non valid numbers.\n");
				return NULL;
			}
		}
	}
	return result;
}


int main(int argc, char *argv[]) {

	if(argc != 4){
		fprintf(stderr, "Use as : %s <orch ip> <port> <op>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	struct addrinfo hints;
	struct addrinfo *result, *rp;
	int sfd, s, i, fdmax, boucle = 1;
	char buf[BUF_SIZE];
	int* resultat = NULL;
	char op = argv[3][0];
	char permitted_op[] = {'+', '-', '*', '/'};
	int nb_permitted_op = 4;
	int flag_op = 0, sendState = 0;
	long int r = 0;
	fd_set master, read_fds;
	struct timeval curTime, oldTime;

	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = 50000;
	FD_ZERO(&master);
	FD_ZERO(&read_fds);

	// looking if the chosen operation is allowed
	for(i = 0; i < nb_permitted_op; i++) {
		if(op == permitted_op[i]) flag_op = 1;
	}
	if(!flag_op) {
		fprintf(stderr,
				"You have to choose a valid operation symbol :\n+, -, * or /\n");
		exit(EXIT_FAILURE);
	}

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
	hints.ai_socktype = SOCK_DGRAM; /* Datagram socket */
	hints.ai_flags = 0;
	hints.ai_protocol = 0;          /* Any protocol */

	//	getaddrinfo returns a bunch of addrinfo structures
	//	we use it to connect our socket to an address	
	s = getaddrinfo(argv[1], argv[2], &hints, &result);
	if (s != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
		exit(EXIT_FAILURE);
	}

	for (rp = result; rp != NULL; rp = rp->ai_next) {
		sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
		if (sfd == -1) continue;

		if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1)
			break;                  /* Success */

		close(sfd);
	}

	if (!rp) {               /* No address succeeded */
		fprintf(stderr, "Could not connect\n");
		exit(EXIT_FAILURE);
	}

	freeaddrinfo(result);           /* No longer needed */

	// setting up file descriptors for select() function
	fdmax = sfd;
	FD_SET(sfd, &master);

	// used for the "check alive" messages
	if(gettimeofday(&oldTime, NULL) == -1) raler("gettimeofday");

	// the - 6 is useful to send a msg right on and not have to wait 5s
	// before the orchestrator notices the node
	oldTime.tv_sec = oldTime.tv_sec - 6;
	srand(time(NULL));

	while (boucle) {
		if(gettimeofday(&curTime, NULL) == -1) raler("gettimeofday");

		read_fds = master;

		if(select(fdmax+1, &read_fds, NULL, NULL, &timeout) == -1) raler("select");

		// if the node receives a message from the orchestrator
		// calculate it and setup random timer
		if(FD_ISSET(sfd, &read_fds)) {
			if(recv(sfd, buf, BUF_SIZE, 0) == -1) {
				close(sfd);
				raler("recv");
			}
			printf("Received:\n%s\n", buf);
			resultat = calculate(buf, op);
			// waits a random time before sending the result
			r = curTime.tv_sec + rand() % 20;
			sendState = 1;
		}

		// sends the result of the calculation to the orchestrator
		// after the random timer is finished
		if(sendState && r - curTime.tv_sec <= 0) {
			memset(buf, '\0', BUF_SIZE);
			if(!resultat){
				buf[0] = 'x';
			} else {
				sprintf(buf, "%d", *resultat);
				free(resultat);
			}
			if(send(sfd, buf, BUF_SIZE, 0) == -1){
				close(sfd);
				raler("send");
			}
			sendState = 0;
		}
		// sends a "check alive" message to the orchestrator
		// if 5 seconds have elapsed
		if(curTime.tv_sec - oldTime.tv_sec >= 5) {
			oldTime = curTime;
			memset(buf, '\0', BUF_SIZE);
			buf[0] = argv[3][0];
			if(send(sfd, buf, BUF_SIZE, 0) == -1) {
				close(sfd);
				raler("send");
			}
		}
	}

	close(sfd);
	exit(EXIT_SUCCESS);
}
