/**
 * \file worker.c
 * \author Guillaume K. & Chloe R.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "worker.h"

/**
 * \fn void raler(char* str)
 * \brief Perror the string and exits with failure
 * \param str The function name to print with perror
 */
void raler(char* str) {
	perror(str);
	exit(EXIT_FAILURE);
}

/**
 * \fn struct worker *is_a_worker(struct worker *w, int nb, char* ip, int port)
 * \brief Return the worker corresponding to the specified port, if it exists
 * \param w Structure containing all the active workers
 * \param nb Number of workers in the structure
 * \param port The port we are going to look for in the structure
 */
struct worker *is_a_worker(struct worker *w, int nb, char* ip, int port) {
	int i;
	for(i=0;i<nb;i++) {
		if(w[i].port == port && !strcmp(w[i].ip, ip)) return &w[i];
	}
	return NULL;
}

/**
 * \fn struct worker* find_op_worker (struct worker * w, int nb, char op)
 * \brief Return the first free worker for the operation
 * \param w Structure containing all the active workers
 * \param nb Number of workers in the structure
 * \param op The type of operation we need to do
 */
struct worker* find_op_worker (struct worker * w, int nb, char op) {
	int i;
	for(i = 0; i < nb; i++) {
		if(!(w[i].working) && (w[i].op == op)) return &w[i];
	}
	return NULL;
}

/**
 * \fn void print_workers(struct worker *w, int nb)
 * \brief Print all the workers from the specified Structure
 * \param w Structure containing all the active workers
 * \param nb Number of workers in the structure
 */
void print_workers(struct worker *w, int nb) {
	int i;
	for(i = 0; i < nb; i++) {
		printf("%s:%d\n  last seen: %lu\n  currently working: %d\n",w[i].ip, 
				w[i].port, w[i].last_seen, w[i].working);
		printf("  operation: %c\n", w[i].op);
	}
	return;
}

/**
 * \fn struct worker *rm_worker(struct worker* w, int *nb, int i)
 * \brief Removes a worker from the list of workers
 * \param w Structure containing all the active workers
 * \param nb Number of workers in the structure
 * \param i Position of the worker to remove
 */
struct worker *rm_worker(struct worker* w, int *nb, int i) {
	free(w[i].ip);
	free(w[i].operation);
	if(i < *nb - 1) {
		w[i] = w[*nb - 1];
	}
	*nb = *nb - 1;
	w = realloc(w, *nb * sizeof(struct worker));
	if(!w && *nb != 0) raler("realloc");

	return w;
}

/**
 * \fn struct worker *check_alive(struct worker* w, int *nb, long ts)
 * \brief Verifies is a worker is still sending the "check alive" messages
 * \param w Structure containing all the active workers
 * \param nb Number of workers in the structure
 */
struct worker *check_alive(struct worker* w, int *nb, long ts) {
	int i;
	for(i=0;i<*nb;i++) {
		if(ts - w[i].last_seen >= 10) {
			printf("worker %s:%d is dead\norchestrator> ", w[i].ip, w[i].port);
			w = rm_worker(w, nb, i);
		}
	}
	return w;
}

/**
 * \fn struct worker *add_worker(struct worker *w,
 *   int *nb, char *ip, int port, time_t ts, char op)
 * \brief Adds a worker to the list of workers
 * \param w Structure containing all the active workers
 * \param nb Number of workers in the structure
 */
struct worker *add_worker(struct worker *w, int *nb, char *ip,
		int port, time_t ts, char op) {

	struct worker a;
	a.port = port;
	a.last_seen = ts;
	a.working = 0;
	*nb = *nb + 1;
	a.op = op;
	a.ip = malloc(strlen(ip)+1);
	if(!(a.operation = malloc(1024))) raler("malloc");

	a.operation = memset(a.operation, '\0', 1024);
	strcpy(a.ip, ip);

	w = realloc(w,*nb * sizeof(struct worker));
	if(!w) raler("realloc");
	w[*nb-1] = a;

	printf("New Worker!\n");
 	printf("It can handle %c operation on <%s,%d>\norchestrator> ", op, ip, port);

	return w;
}


