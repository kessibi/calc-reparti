#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <time.h>
#include "../include/worker.h"

#define BUF_SIZE 4096

int main(int argc, char *argv[]) {

	struct addrinfo hints, *result, *rp; // Useful for getaddrinfo
	struct sockaddr_storage peer_addr; // Get worker informations
	socklen_t peer_addr_len;
	struct sockaddr_in6 *t; // to retrieve ip address from worker
	struct in6_addr ipa;

	fd_set master, read_fds;
	FD_ZERO(&master);
	FD_ZERO(&read_fds);

	struct tm *tminfo;
	struct timeval tv; // used to check if workers died, attach the date to
										 //a calculation, print the date when retrieving calutation
	struct timeval timeout; // used for select timeout
	timeout.tv_sec = 0;
	timeout.tv_usec = 50000;

	char buf[BUF_SIZE];
	int fdmax;
	int sfd, s, res, boucle = 1;
	int nbworkers = 0;
	// w represents the list of workers
	// work is a pointer to a single worker

	struct worker *w = NULL, *work = NULL;
	char straddr[INET6_ADDRSTRLEN];
	char ope[BUF_SIZE];
	char host[NI_MAXHOST], port[NI_MAXSERV];



	if (argc != 2) {
		fprintf(stderr, "Usage: %s port\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_INET6;
	hints.ai_socktype = SOCK_DGRAM; /* Datagram socket */
	hints.ai_flags = AI_PASSIVE;    /* For wildcard IP address */
	hints.ai_protocol = 0;          /* Any protocol */
	hints.ai_canonname = NULL;
	hints.ai_addr = NULL;
	hints.ai_next = NULL;

	// IP connection, getaddrinfo returns a bunch of struct addrinfo
	// we'll use these to connect a socket to an address
	// as seen in the man page
	s = getaddrinfo(NULL, argv[1], &hints, &result);
	if (s != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
		exit(EXIT_FAILURE);
	}

	// Run through the list to find one that fits (as seen in man page)
	for (rp = result; rp != NULL; rp = rp->ai_next) {
		sfd = socket(rp->ai_family, rp->ai_socktype,
				rp->ai_protocol);
		if (sfd == -1)
			continue;

		if (bind(sfd, rp->ai_addr, rp->ai_addrlen) == 0)
			break;                  /* Success */

		close(sfd);
	}

	if (!rp) {               /* No address succeeded */
		fprintf(stderr, "Could not bind\n");
		exit(EXIT_FAILURE);
	}
	freeaddrinfo(result);           /* No longer needed */

	// Set fdset with the socket fd and stdin fd
	FD_SET(sfd, &master);
	FD_SET(STDIN_FILENO, &master);

	fdmax = sfd;

	// on some linux distros it is necessary for printing without the \n
	setbuf(stdout, NULL);
	printf("orchestrator> ");

	while (boucle) {
		// used to know if a node is dead
		gettimeofday(&tv, NULL);

		read_fds = master;

		if(select(fdmax+1, &read_fds, NULL, NULL, &timeout) == -1) {
			close(sfd);
			raler("select");
		}

		//  Read STDIN
		// receives a calculation
		if(FD_ISSET(STDIN_FILENO, &read_fds)) {
			memset(ope, '\0', BUF_SIZE);
			scanf("%s", ope);

			printf("\033[1A"); //   go to the line on top
			printf("\033[50C"); //  move from 50 to the right

			// if the user writes "quit", leave the loop for proper program ending
			if(!strcmp(ope, "quit")) {
				boucle = 0;
			// else check if the first character of the input
			// corresponds to an operation supported by a known worker, print,
			// send, ...
			// else: inform that no worker was found for said operation
			} else if(!(work = find_op_worker(w, nbworkers, ope[0]))) {
				fprintf(stderr, "\nNo worker found for the %c operation\n",
						ope[0]);
				printf("orchestrator> ");
			} else {
				strcpy(work->operation, ope);
				work->envoi = tv.tv_sec;
				tminfo = localtime(&tv.tv_sec);
				printf("# demande effectuee a: %d:%d:%d\n",
						tminfo->tm_hour,
						tminfo->tm_min,
						tminfo->tm_sec );
				printf("orchestrator> ");

				t = (struct sockaddr_in6*) &peer_addr;
				t->sin6_family = AF_INET6;
				t->sin6_port = htons(work->port);
				if(inet_pton(AF_INET6, work->ip, &t->sin6_addr) != 1) {
					close(sfd);
					raler("inet_pton");
				}
				// sends the operation to the node
				if (sendto(sfd, ope, BUF_SIZE, 0,
							(struct sockaddr *) t,
							sizeof(struct sockaddr_in6)) != BUF_SIZE)
					fprintf(stderr, "Error sending response\n");
				// the node is now calculating
				work->working = 1;
			}

		}

		//  Read socket
		if(FD_ISSET(sfd, &read_fds)) {
			peer_addr_len = sizeof(struct sockaddr_storage);
			// receives a message from a node
			if(recvfrom(sfd, buf, BUF_SIZE, 0,
						(struct sockaddr *) &peer_addr, &peer_addr_len) == -1) {
				close(sfd);
				raler("recvfrom");
			}

			s = getnameinfo((struct sockaddr *) &peer_addr,
					peer_addr_len, host, NI_MAXHOST,
					port, NI_MAXSERV, NI_NUMERICSERV);

			if(s != 0) {
				close(sfd);
				fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
				exit(EXIT_FAILURE);
			} else {

				t = (struct sockaddr_in6*) &peer_addr;
				ipa = t->sin6_addr;
				if(!inet_ntop(AF_INET6, &ipa, straddr, INET6_ADDRSTRLEN)) {
					close(sfd);
					raler("inet_ntop");
				}
				// if the node is not in the list yet
				// the orchestrator adds it
				if(!(work = is_a_worker(w, nbworkers, straddr, atoi(port)))) {
					w = add_worker(w, &nbworkers, straddr, atoi(port),
							tv.tv_sec, buf[0]);

					// it is a message from an already known node
				} else {
					// reinitialises the check alive date
					work->last_seen = tv.tv_sec;

					// in the case where the operation was not valid
					if(buf[0] == 'x') {
						printf("The operation was not valid.\n");
						printf("orchestatror> ");
						work->working = 0;
					}
					// in the case where the operation was valid
					// extraction of the result
					else if(buf[0] != work->op || buf[1] != '\0') {
						sscanf(buf, "%d", &res);
						work->working = 0;
						tminfo = localtime(&tv.tv_sec);
						printf("\n%d:%d:%d ",
								tminfo->tm_hour,
								tminfo->tm_min,
								tminfo->tm_sec );
						printf("\x1B[35m%s = %d\x1B[0m", work->operation, res);
						printf(" by <%s,%d> at ", work->ip, work->port);
						tminfo = localtime(&work->envoi);
						printf("%d:%d:%d ",
								tminfo->tm_hour,
								tminfo->tm_min,
								tminfo->tm_sec );
						printf("\n");
						printf("orchestrator> ");

					}
				}
			}
		}
		// check if there any node died during the process
		w = check_alive(w, &nbworkers, tv.tv_sec);
	}

	while(w) w = rm_worker(w, &nbworkers, 0);
	putchar('\n');
	exit(0);
}
