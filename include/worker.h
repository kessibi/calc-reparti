#ifndef _WORKER
#define _WORKER

#include <time.h>

/**
* \struct worker orchestrator.c
* \brief Structure for each active worker know by the orchestrator
*/
struct worker {
    char *ip; /**< Version of IP used by the worker */
    int port; /**< Port on which the worker is sending and receiving messages */
    time_t last_seen; /**
		       * < The last time this worker send a "check alive" message 
		       */
    int working; /**< Boolean to know if the worker is working */
    char op; /**< Operation realised by the worker */
    char *operation; /** < The operation given to a worker upon request */
    time_t envoi; /**  < Time at which the request is sent */
};

void raler(char* str);

struct worker *is_a_worker(struct worker *w, int nb, char* ip, int port);

struct worker* find_op_worker (struct worker * w, int nb, char op);

void print_workers(struct worker *w, int nb);

struct worker *rm_worker(struct worker* w, int *nb, int i);

struct worker *check_alive(struct worker* w, int *nb, long ts);

struct worker *add_worker(struct worker *w, int *nb, char *ip,
		int port, time_t ts, char op);

#endif
